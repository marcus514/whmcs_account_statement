{if $licence}
    <div class="alert alert-danger">{$message}</div>
{else}
    <script type="text/javascript" src="{$BASE_PATH_JS}/jquery-ui.min.js"></script>
    <link href="{$BASE_PATH_CSS}/jquery-ui.min.css" rel="stylesheet" type="text/css" />
    {if $template eq 'five'}
        <link href="{$BASE_PATH_CSS}/font-awesome.css" rel="stylesheet" type="text/css" />
        <link href="{$baseurl}/modules/addons/accountStatement/css/fivestyle.css" rel="stylesheet" type="text/css" />
    {/if}
    <script type="text/javascript">
        var datepickerformat = "dd/mm/yy";
        jQuery(document).ready(function() {
            jQuery("#startdate").datepicker({
                dateFormat: "yy/mm/dd"
            });
            jQuery("#enddate").datepicker({
                dateFormat: "yy/mm/dd"
            });
        });
        function resetfrm() {
            jQuery("#startdate").val("");
            jQuery("#enddate").val("");
        }
    </script>
    <style>
        .page-header{ text-align: center; border-bottom: none;}
        .customrow{ width: 600px; margin: 0 auto; clear: both; }
    </style>
    <div class="page-header">
        <div class="styled_title"><h1>{$pagetitle}</h1></div>
    </div>
    <div id="maincontent">
        <div id="innercontainer">
            <form class="form" action="" method="post" id="filterfrm" class="center95 form-stacked">
                <input type="hidden" name="action" value="genrate_client_statement" />
                <input type="hidden" name="userid" value="{$userid}" />
                <div class="row"> 
                    <div class="customrow">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="startdate" class="control-label">Start Date</label>
                                <input type="text" name="startdate" id="startdate" value="{$statements.startdate}" class="form-control datepick">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="enddate" class="control-label">End Date</label>
                                <input type="text" name="enddate" id="enddate" value="{$statements.enddate}" class="form-control datepick">
                            </div>
                        </div>

                        <div class="form-group text-center">
                            <div class="col-sm-6">
                                <div class="form-group">                                
                                    <select name="invoicetype" class="form-control" style="width:100%;">
                                        <option value="all"{if $statements.invoicetype eq 'all'} selected="selected"{/if}>Invoice All</option>
                                        <option value="paid"{if $statements.invoicetype eq 'paid'} selected="selected"{/if}>Paid</option>
                                        <option value="unpaid"{if $statements.invoicetype eq 'unpaid'} selected="selected"{/if}>Unpaid</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input class="btn btn-primary"  style="width:100%;" type="submit" value="Search My Statement">
                                </div>
                            </div>                                
                        </div>
                    </div>
                </div>                 
            </form>            
            <br/><br/>
            
            
            {if $statements}
<div class="table-container clearfix">
    <div id="tableInvoicesList_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
        <div class="listtable">
            <div class="dataTables_info" id="tableInvoicesList_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries</div>
           
                <table id="tableInvoicesList" class="table table-list dataTable no-footer dtr-inline" aria-describedby="tableInvoicesList_info" role="grid">
                    <tr>
                        <th width="5%">S.No</th>
                        <th>Title</th>
                        <th width="10%"></th>
                        <th width="10%"></th>
                    </tr>
                    {if $statements.result eq 'success'}
                        <tr class="odd" style="cursor: inherit;">
                            <td class="text-center">1</td>
                            <td class="text-center" style="word-break: break-all;"><strong>{$statements.file}</strong></td>
                            <td class="text-center"><a href="{$statements.url}" target="_blank"><img src="modules/addons/accountStatement/img/view.png" /></a></td>
                            <td class="text-center"><a href="{$statements.downloadlink}"><img src="modules/addons/accountStatement/img/file-download.png" /></a></td>            
                        </tr>
                    {else}
                        <tr class="odd" style="cursor: inherit;">
                            <td class="text-center" colspan="4"><strong>{$statements.message}</strong></td>
                        </tr>
                    {/if}
                </table>
            
        </div>
    </div>    
</div>
                {/if}
            
            
        </div>
    </div>
{/if}





