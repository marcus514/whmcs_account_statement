<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class AccountSummary {

    public function get_clients_registration_date() {
        $result = array();
        $sql = Capsule::table("tblclients")->get();
        foreach ($sql as $row) {
            $result[] = (array) $row;
        }
        return $result;
    }

    public function get_due_invoces($userid, $startdate, $enddate) {

        $result = array();
        $invoicetype = $this->pdfInvoiceType();
        $data = $this->accountStatement_config();
        if ($data['IncludeUnpaidInvoices'] == 'on' && $invoicetype != "paid") {
            $sql = "SELECT * FROM `tblinvoices` WHERE `tblinvoices`.`status` = 'Unpaid' AND `tblinvoices`.`userid` = '" . $userid . "'  ORDER BY  `tblinvoices`.`id` ASC ";
            $query1 = Capsule::select($sql);
            foreach ($query1 as $queryData1) {
                if (strtotime($queryData1->date) < strtotime($enddate) || strtotime($queryData1->date) > strtotime($startdate)) {
                    $query[] = $queryData1;
                }
            }
        }
        if ($invoicetype == "unpaid") {
            $sql = "SELECT * FROM `tblinvoices` WHERE `tblinvoices`.`status` = 'Unpaid' AND `tblinvoices`.`userid` = '" . $userid . "' AND `tblinvoices`.`date` >= '" . $enddate . "' AND `tblinvoices`.`date` <= '" . $startdate . "' ORDER BY  `tblinvoices`.`id` ASC ";
            $query1 = Capsule::select($sql);
            foreach ($query1 as $queryData1) {
                $query[] = $queryData1;
            }
        } elseif ($invoicetype == "paid") {

            $query = array();
            $sql = "SELECT * FROM `tblinvoices` WHERE `tblinvoices`.`status` = 'Paid' AND `tblinvoices`.`userid` = '" . $userid . "' AND `tblinvoices`.`date` >= '" . $enddate . "' AND `tblinvoices`.`date` <= '" . $startdate . "' ORDER BY  `tblinvoices`.`id` ASC ";
            $query1 = Capsule::select($sql);
            foreach ($query1 as $queryData1) {
                $query[] = $queryData1;
            }

            $sql = "SELECT * FROM `tblinvoices` WHERE `tblinvoices`.`status` = 'Unpaid' AND `tblinvoices`.`userid` = '" . $userid . "' AND `tblinvoices`.`date` >= '" . $enddate . "' AND `tblinvoices`.`date` <= '" . $startdate . "' and `tblinvoices`.`id` IN (select invoiceid FROM tblaccounts) ORDER BY  `tblinvoices`.`id` ASC ";
            $query2 = Capsule::select($sql);
            foreach ($query2 as $queryData2) {
                $query[] = $queryData2;
            }
        } else {
            $sql = "SELECT * FROM `tblinvoices` WHERE (`tblinvoices`.`status` = 'Unpaid' OR `tblinvoices`.`status` = 'Paid') AND `tblinvoices`.`userid` = '" . $userid . "' AND `tblinvoices`.`date` >= '" . $enddate . "' AND `tblinvoices`.`date` <= '" . $startdate . "' ORDER BY  `tblinvoices`.`id` ASC ";
            $query1 = Capsule::select($sql);
            foreach ($query1 as $queryData1) {
                $query[] = $queryData1;
            }
        }

        if (!empty($query)) {
            foreach ($query as $row) {
                $qry = Capsule::table("tblinvoiceitems")->where("invoiceid", $row->id)->get();
                $count = 0;
                $mcount = 0;
                foreach ($qry as $row1) {
                    $row1 = (array) $row1;
                    $result[$row->id]['items'][$count] = $row1;
                    if ($row1["type"] == "Invoice") {
                        $result[$row->id]['masspay'][$mcount] = $row1["relid"];
                        $mcount++;
                    }
                    $count++;
                }
                $result[$row->id]['number'] = $row->invoicenum;
                $result[$row->id]['create'] = $row->date;
                $result[$row->id]['duedate'] = $row->duedate;
                $result[$row->id]['subtotal'] = $row->subtotal;
                $result[$row->id]['credit'] = $row->credit;
                $result[$row->id]['total'] = $row->total;
            }
        }
        return $result;
    }

    public function get_due_invoces_manually($userid, $startdate, $enddate) {

        $result = array();
        $invoicetype = $this->pdfInvoiceType();
        if ($invoicetype == "unpaid") {
            $sql = "SELECT * FROM `tblinvoices` WHERE `tblinvoices`.`status` = 'Unpaid' AND `tblinvoices`.`userid` = '" . $userid . "' AND `tblinvoices`.`date` >= '" . $enddate . "' AND `tblinvoices`.`date` <= '" . $startdate . "' ORDER BY  `tblinvoices`.`id` ASC ";
            $query1 = Capsule::select($sql);
            foreach ($query1 as $queryData1) {
                $query[] = $queryData1;
            }
        } elseif ($invoicetype == "paid") {

            $query = array();
            $sql = "SELECT * FROM `tblinvoices` WHERE `tblinvoices`.`status` = 'Paid' AND `tblinvoices`.`userid` = '" . $userid . "' AND `tblinvoices`.`date` >= '" . $enddate . "' AND `tblinvoices`.`date` <= '" . $startdate . "' ORDER BY  `tblinvoices`.`id` ASC ";
            $query1 = Capsule::select($sql);
            foreach ($query1 as $queryData1) {
                $query[] = $queryData1;
            }

            $sql = "SELECT * FROM `tblinvoices` WHERE `tblinvoices`.`status` = 'Unpaid' AND `tblinvoices`.`userid` = '" . $userid . "' AND `tblinvoices`.`date` >= '" . $enddate . "' AND `tblinvoices`.`date` <= '" . $startdate . "' and `tblinvoices`.`id` IN (select invoiceid FROM tblaccounts) ORDER BY  `tblinvoices`.`id` ASC ";
            $query2 = Capsule::select($sql);
            foreach ($query2 as $queryData2) {
                $query[] = $queryData2;
            }
        } else {
            $sql = "SELECT * FROM `tblinvoices` WHERE (`tblinvoices`.`status` = 'Unpaid' OR `tblinvoices`.`status` = 'Paid') AND `tblinvoices`.`userid` = '" . $userid . "' AND `tblinvoices`.`date` >= '" . $enddate . "' AND `tblinvoices`.`date` <= '" . $startdate . "' ORDER BY  `tblinvoices`.`id` ASC ";
            $query1 = Capsule::select($sql);
            foreach ($query1 as $queryData1) {
                $query[] = $queryData1;
            }
        }

        if (!empty($query)) {
            foreach ($query as $row) {
                $qry = Capsule::table("tblinvoiceitems")->where("invoiceid", $row->id)->get();
                $count = 0;
                $mcount = 0;
                foreach ($qry as $row1) {
                    $row1 = (array) $row1;
                    $result[$row->id]['items'][$count] = $row1;
                    if ($row1["type"] == "Invoice") {
                        $result[$row->id]['masspay'][$mcount] = $row1["relid"];
                        $mcount++;
                    }
                    $count++;
                }
                $result[$row->id]['number'] = $row->invoicenum;
                $result[$row->id]['create'] = $row->date;
                $result[$row->id]['duedate'] = $row->duedate;
                $result[$row->id]['subtotal'] = $row->subtotal;
                $result[$row->id]['credit'] = $row->credit;
                $result[$row->id]['total'] = $row->total;
            }
        }
        return $result;
    }
    
    public function get_all_invoces($userid, $startdate, $enddate, $invoicetype) {
        $result = array();
        if ($invoicetype == "paid") {
            $query = array();
            $sql = "SELECT * FROM `tblinvoices` WHERE `tblinvoices`.`status` = 'Paid' AND `tblinvoices`.`userid` = '" . $userid . "' AND `tblinvoices`.`date` >= '" . $enddate . "' AND `tblinvoices`.`date` <= '" . $startdate . "' ORDER BY  `tblinvoices`.`id` ASC ";
            $query1 = Capsule::select($sql);
            foreach ($query1 as $queryData1) {
                $query[] = $queryData1;
            }
            $sql = "SELECT * FROM `tblinvoices` WHERE `tblinvoices`.`status` = 'Unpaid' AND `tblinvoices`.`userid` = '" . $userid . "' AND `tblinvoices`.`date` >= '" . $enddate . "' AND `tblinvoices`.`date` <= '" . $startdate . "' and `tblinvoices`.`id` IN (select invoiceid FROM tblaccounts) ORDER BY  `tblinvoices`.`id` ASC ";
            $query2 = Capsule::select($sql);
            foreach ($query2 as $queryData2) {
                $query[] = $queryData2;
            }
        } elseif ($invoicetype == "unpaid") {
            $sql = "SELECT * FROM `tblinvoices` WHERE `tblinvoices`.`status` = 'Unpaid' AND `tblinvoices`.`userid` = '" . $userid . "' AND `tblinvoices`.`date` >= '" . $enddate . "' AND `tblinvoices`.`date` <= '" . $startdate . "' ORDER BY  `tblinvoices`.`id` ASC ";
            $query1 = Capsule::select($sql);
            foreach ($query1 as $queryData1) {
                $query[] = $queryData1;
            }
        } elseif ($invoicetype == "all") {
            $sql = "SELECT * FROM `tblinvoices` WHERE (`tblinvoices`.`status` = 'Unpaid' OR `tblinvoices`.`status` = 'Paid') AND `tblinvoices`.`userid` = '" . $userid . "' AND `tblinvoices`.`date` >= '" . $enddate . "' AND `tblinvoices`.`date` <= '" . $startdate . "' ORDER BY  `tblinvoices`.`id` ASC ";
            $query1 = Capsule::select($sql);
            foreach ($query1 as $queryData1) {
                $query[] = $queryData1;
            }
        }

        if (!empty($query)) {
            foreach ($query as $row) {
                $qry = Capsule::table("tblinvoiceitems")->where("invoiceid", $row->id)->get();
                $count = 0;
                foreach ($qry as $row1) {
                    $row1 = (array) $row1;
                    $result[$row->id]['items'][$count] = $row1;
                    if ($row1["type"] == "Invoice") {
                        $result[$row->id]['masspay'][$count] = $row1["relid"];
                    }
                    $count++;
                }
                $result[$row->id]['create'] = $row->date;
                $result[$row->id]['duedate'] = $row->duedate;
                $result[$row->id]['subtotal'] = $row->subtotal;
                $result[$row->id]['credit'] = $row->credit;
                $result[$row->id]['total'] = $row->total;
            }
        }
        return $result;
    }

    public function invoice_transaction_details($invoiceid, $userid) {
        $result = array();
        $sql = "SELECT `gateway`, `date`, `amountin`, `amountout`, `transid` FROM  `tblaccounts` WHERE `userid` = '" . $userid . "' AND `invoiceid` = '" . $invoiceid . "' ORDER BY  `tblaccounts`.`id` ASC ";
        $query = Capsule::select($sql);
        foreach ($query as $row) {
            $row = (array) $row;
            $result[] = $row;
        }
        return $result;
    }

    public function get_companyAddress() {
        $addressArr = array();
        $row = Capsule::table("tblconfiguration")->where("setting", "InvoicePayTo")->get();

        if (!empty($row)) {
            $addressArr = explode("\n", $row[0]->value);
        } else {
            $addressArr[0] = "Address goes here";
        }
        return $addressArr;
    }

    public function get_clientdetails($userid) {
        $row = Capsule::table("tblclients")->select('firstname', 'lastname', 'companyname', 'address1', 'address2', 'city', 'state', 'postcode', 'country')->where("id", $userid)->get();
        $row = (array) $row[0];
        return $row;
    }

    public function add_attachments($userid, $pdfname) {
        $sql = Capsule::table("mod_account_summary_pdf")->where("userid", $userid)->count();
        if ($sql > 0) {
            Capsule::table("mod_account_summary_pdf")->where("userid", $userid)->update(array('title' => $pdfname));
        } else {
            Capsule::table("mod_account_summary_pdf")->insert(array('title' => $pdfname, 'userid' => $userid));
        }
    }

    public function remove_attachments($userid) {
        $sql = Capsule::table("mod_account_summary_pdf")->select('title')->where("userid", $userid)->first();
        $pdfname = $sql->title;
        $upOne = realpath(__DIR__ . '/..');
        $file = $upOne . '/attachments/' . $pdfname;
        if (is_file($file)) {
            unlink($file); // delete file
        }
    }

    public function downloadstatement($userid) {
        $sql = Capsule::table("mod_account_summary_pdf")->select('title')->where("userid", $userid)->get();
        $pdfname = $sql[0]->title;
        $upOne = realpath(__DIR__ . '/..');
        $file = $upOne . '/attachments/' . $pdfname;
        if (file_exists($file)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($file));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file));
            ob_clean();
            flush();
            readfile($file);
            exit();
        }
    }

    public function companydetails() {
        $result = array();
        $sql = "SELECT * FROM `tblconfiguration` WHERE `setting` IN ('CompanyName','Email')";
        $query = Capsule::select($sql);
        //while ($row = mysql_fetch_assoc($query)) {
        foreach ($query as $row) {
            if ($row['setting'] == "CompanyName") {
                $result['name'] = $row->value;
            } else {
                $result['email'] = $row->value;
            }
        }
        return $result;
    }

    public function clientemailID($userid) {
        $query = Capsule::table("tblclients")->select('email')->where("id", $userid)->get();
        $emailid = $query[0]->email;
        return $emailid;
    }

    public function get_attachment($userid) {
        $query = Capsule::table("mod_account_summary_pdf")->select('title')->where("userid", $userid)->first();
        $pdfname = $query->title;
        return $pdfname;
    }

    public function invoice_amount($invoiceid) {
        $query = Capsule::table("tblinvoices")->where("id", $invoiceid)->first();
        $total = $query->total;
        $credit = $query->credit;
        if ($total == 0.00) {
            $total = $query->credit . "*c";
        } elseif ($total > 0 && $credit > 0) {
            $total = $total . "*p" . $credit;
        }
        return $total;
    }

    public function insert_configuration() {
        $sql = Capsule::table("mod_account_summary_configuration")->count();
        if ($sql == 0) {
            $fields = array(
                "PDFPaperSize" => "A4",
                "TCPDFFont" => "helvetica",
                "TCPDFCoustomFont" => '',
                "EnablePDFInvoices" => 'on',
                "PDFInvoiceType" => 'unpaid'
            );

            foreach ($fields as $setting => $value) {
                Capsule::table("mod_account_summary_configuration")->insert(array('setting' => $setting, 'value' => $value, 'created_at' => date('Y-m-d h:i:s')));
            }
        }
    }

    public function accountStatement_config() {
        $result = array();
        $query = Capsule::table("mod_account_summary_configuration")->get();
        foreach ($query as $row) {
            $result[$row->setting] = $row->value;
        }
        return $result;
    }

    public function update_configuration($POST) {
        if ($POST['enablepdfinvoices'] == 'on') {
            $fieldtype = $POST['pdfinvoicetype'];
        } else {
            $fieldtype = '';
        }

        $dirpath = str_replace('class', '', __DIR__);

        $oldFontData = Capsule::table("mod_account_summary_configuration")->select('value')->where('setting', 'TCPDFCoustomFont')->get();
        $oldFile = '';
        if (!empty($oldFontData[0]->value)) {
            $oldFile = $oldFontData[0]->value;
        }
        if ($POST['tcpdffont'] != 'custom') {
            unlink($dirpath . '/font/' . $oldFile);
        }

        if ($POST['tcpdffont'] == 'custom' && !empty($_FILES["tcpdffontcustom"]["name"])) {

            $pdfcustomfont = $_FILES["tcpdffontcustom"]["name"];

            $allowed = array('Ttf', 'TTF', 'ttf');
            $filename = $pdfcustomfont;
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if (!in_array($ext, $allowed)) {
                return 'You have uploaded wrong file, Please upload only font file (font.ttf).';
            }

            unlink($dirpath . '/font/' . $oldFile);
            move_uploaded_file($_FILES["tcpdffontcustom"]["tmp_name"], $dirpath . '/font/' . $pdfcustomfont);
        } else {
            $pdfcustomfont = '';
        }
        $fields = array(
            "PDFPaperSize" => $POST['pdfpapersize'],
            "TCPDFFont" => $POST['tcpdffont'],
            "TCPDFCoustomFont" => $pdfcustomfont,
            "EnablePDFInvoices" => $POST['enablepdfinvoices'],
            "PDFInvoiceType" => $fieldtype,
            "EnableAutoStatement" => $POST['enableautostatement'],
            "IncludeUnpaidInvoices" => $POST['includeallunpaidinvoices'],
        );
        foreach ($fields as $setting => $value) {
            $query = Capsule::table("mod_account_summary_configuration")->where('setting', $setting)->first();
            if (empty($query->setting)) {
                Capsule::table("mod_account_summary_configuration")->insert(array('setting' => $setting, 'value' => $value, 'created_at' => date('Y-m-d h:i:s')));
            } else {
                Capsule::table("mod_account_summary_configuration")->where('setting', $setting)->update(array('value' => $value, 'updated_at' => date('Y-m-d h:i:s')));
            }
        }
    }

    public function getPdfFont() {
        $query = Capsule::table("mod_account_summary_configuration")->select('value')->where('setting', 'TCPDFFont')->first();
        $result = $query->value;
        return $result;
    }

    public function pdfFont() {
        $query = Capsule::table("mod_account_summary_configuration")->select('value')->where('setting', 'TCPDFFont')->first();
        $result = $query->value;
        if ($result == "custom") {
            $pdffont = $this->pdfcustomfont();
        } else {
            $pdffont = $result;
        }
        return $pdffont;
    }

    public function pdfcustomfont() {
        $query = Capsule::table("mod_account_summary_configuration")->select('value')->where('setting', 'TCPDFCoustomFont')->first();
        $result = $query->value;
        return $result;
    }

    public function pdfPaperSize() {
        $query = Capsule::table("mod_account_summary_configuration")->select('value')->where('setting', 'PDFPaperSize')->first();
        $result = $query->value;
        return $result;
    }

    public function pdfInvoiceType() {
        $query = Capsule::table("mod_account_summary_configuration")->select('value')->where('setting', 'PDFInvoiceType')->first();
        $result = $query->value;
        return $result;
    }

    public function invoicetransctions($invoiceid, $userid) {
        $result = array();
        $sql = "SELECT `date`, `amountin`, `amountout`, `transid`,`refundid` FROM `tblaccounts` WHERE `invoiceid` = '" . $invoiceid . "' ORDER BY  `tblaccounts`.`id` ASC ";
        $query = Capsule::select($sql);
        foreach ($query as $row) {
            $row = (array) $row;
            if ($row['amountin'] == '0.00' && $row['amountout'] != '0.00') {
                $row['amountin'] = $row['amountin'] - $row['amountout'];
            }
            $result[] = $row;
        }
        if (count($query) == 0) {
            $query = Capsule::table('tblcredit')->where('clientid', $userid)->where('description', 'like', '%' . $invoiceid)->first();
            if (!empty($query->id)) {
                $paidamount = $query->amount;
                if ($query->amount < 0.00) {
                    $paidamount = -($paidamount);
                }
                $result[] = array(
                    "date" => $query->date,
                    "amountin" => $paidamount,
                    "amountout" => 0.00,
                    "transid" => "Credit Applied",
                    "refundid" => 0
                );
            }
        }
        return $result;
    }

    public function addEmailTemplates($POST) {
        $template = 'Account Statement';
        $message = html_entity_decode($POST['message'], ENT_COMPAT | ENT_HTML5, 'utf-8');
        $sql = Capsule::table("tblemailtemplates")->where('name', $template)->where('type', 'general')->count();
        if ($sql > 0) {
            Capsule::table("tblemailtemplates")->where('name', $template)->where('type', 'general')->update(array('subject' => $POST['subject'], 'message' => $message, 'fromname' => $POST['fromname'], 'fromemail' => $POST['fromemail'], 'copyto' => $POST['copyto'], 'updated_at' => date('Y-m-d h:i:s')));
        } else {
            Capsule::table("tblemailtemplates")->insert(array('type' => 'general', 'name' => $template, 'subject' => $POST['subject'], 'message' => $message, 'fromname' => $POST['fromname'], 'fromemail' => $POST['fromemail'], 'copyto' => $POST['copyto'], 'created_at' => date('Y-m-d h:i:s')));
        }
    }

    public function getEmailTemplates() {
        $template = 'Account Statement';
        $sql = Capsule::table("tblemailtemplates")->where('name', $template)->where('type', 'general')->get();
        $result = (array) $sql[0];
        return $result;
    }

    public function checkSendStatement() {
        $sql = Capsule::table("mod_account_summary_configuration")->select('value')->where('setting', 'EnablePDFInvoices')->first();
        $result = $sql->value;
        return $result;
    }

    public function checkAutomaticMonthlyStatement() {
        $sql = Capsule::table("mod_account_summary_configuration")->select('value')->where('setting', 'EnableAutoStatement')->first();
        $result = $sql->value;
        return $result;
    }

    public function updateAttachments($attachments) {
        $template = 'Account Statement';
        Capsule::table("tblemailtemplates")->where('name', $template)->where('type', 'general')->update(array('attachments' => $attachments));
    }

    public function getAdmin() {
        $sql = Capsule::table("tbladmins")->select('id')->take(1)->first();
        $result = $sql->username;
        return $result;
    }

    public function remove_attachments_downloads($pdfname) {
        $template = 'Account Statement';
        Capsule::table("tblemailtemplates")->where('name', $template)->where('type', 'general')->update(array('attachments' => ''));
        $basepath = realpath(__DIR__ . '/../../../..');
        $file = $basepath . "/downloads/" . $pdfname;
        if (is_file($file)) {
            unlink($file); // delete file
        }
    }

    public function customFormatCurrency($amount, $userid) {
        $record = Capsule::table("tblclients")->where('id', $userid)->first();
        $currency = $record->currency;
        $record = Capsule::table("tblcurrencies")->where('id', $currency)->first();
        if (!$amount) {
            $amount = "0.00";
        }
        if ($amount < 0) {
            $amount = -($amount);
            return "-" . $record->prefix . $amount . " " . $record->suffix;
        } else {
            return $record->prefix . $amount . " " . $record->suffix;
        }
    }

    public function pcustomFormatCurrency($amount, $userid) {
        $record = Capsule::table("tblclients")->where('id', $userid)->first();
        $currency = $record->currency;
        $record = Capsule::table("tblcurrencies")->where('id', $currency)->first();
        if (!$amount) {
            $amount = "0.00";
        }
        if ($amount < 0) {
            $amount = -($amount);
            return "-" . $record->prefix . $amount;
        } else {
            return $record->prefix . $amount;
        }
    }

}