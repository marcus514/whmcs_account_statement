version 1.7
Product Summary & Statement Modules:-
  
   The account statement modules use to send all statements to users every month on their anniversary date.

Configuration:

 Admin Area:
   Step 1. Upload accountStatement folder to modules/addons.
   Step 2. Login to whmcs admin panel and goto setup > Addon Modules,
          Activate and Configure "Addon Statements" with required permissions.
   Step 3. Go to Addons -> Account Statement to configure email template and PDF options.

New links will be adding in the client billing menus, if you need to add links elsewhere then use as follows.
   Link:  index.php?m=accountStatement 

If you want to change how the statement looks, then edit the file accountstatementpdf.php

     
  