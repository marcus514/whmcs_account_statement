<?php

use Illuminate\Database\Capsule\Manager as Capsule;
use WHMCS\View\Menu\Item as MenuItem;

// Generate automatically account statements in anniversary data. 
function account_summary_sendstatements() {
    global $_LANG;
    require_once __DIR__ . '/class/class.accountsummary.php';         // Include addon class file 
    $accountsummary = new AccountSummary();
    $temp = false;
    $registerations = $accountsummary->get_clients_registration_date();   // Get all clients registration date
    foreach ($registerations as $register) {
        $registerdate = date("d", strtotime($register["datecreated"]));
        $currentdate = date("d");
        if ($currentdate == $registerdate) {
            $checkautomonthlystatement = $accountsummary->checkAutomaticMonthlyStatement();
            if ($checkautomonthlystatement == "on") {
                $userid = $register["id"];
                $sdt = date('Y-m-d') . ' 00:00:00';
                $edt = date('Y-m-d') . ' 23:59:59';
                $templaterecord = Capsule::table("tblemailtemplates")->where('name', 'Account Statement')->first();
                $emailrecord = Capsule::table("tblemails")->where('userid', $userid)->where('subject', $templaterecord->subject )->whereBetween('tblemails.date', array($sdt, $edt))->get();
                if (count($emailrecord) == 0 ) {
                    $enddate = date("Y-m-d", strtotime("-1 months"));
                    $startdate = date("Y-m-d");
                    $dueinvoices = $accountsummary->get_due_invoces($userid, $startdate, $enddate);  // Get client all outstanding invoices
                    if (!empty($dueinvoices)) {
                        $pdf = new TCPDF();
                        require_once 'accountstatementpdf.php';         // Generate account statement pdf 
                        if ($temp) {
                            account_summary_sendphpmailer($userid, "system");     // Send account statement 
                        }
                    } else {
                        $description = 'Cron Job: No outstanding invoices avaliable Email status: Success ' . 'User ID: ' . $userid;
                        logActivity($description);
                    }
                }
            }
        }
    }
}

function add_account_statements_invoicetab($vars) {          // Admin area headoutput hook to create statement btn in invoice tab
    if ($vars['filename'] == "clientsinvoices" && isset($_GET['userid']) && !empty($_GET['userid'])) {
        $userid = $_GET['userid'];
        global $CONFIG;
        $moduleURL = $CONFIG['SystemURL'] . '/modules/addons/accountStatement/';    // Module base url
        $script = <<<javascript
                <script type="text/javascript">
                    jQuery(document).ready(function(){
                        jQuery("#profileContent").find("form").eq(0).find("div").append('<button type="button" id="statementbtn" class="btn btn-success"><i class=\'fa fa-file-text-o\'></i>&nbsp;Statement</button>');
                        jQuery("#profileContent").find("form").eq(0).after('<div id="addstaementshtml" style="display:none;"><div>');
                        jQuery("#profileContent").find("form").eq(1).after('<div id="statementtbl"></div>');
                        jQuery("body").on("click","#statementbtn",function(){
                             var userid = '$userid';
                            jQuery.post("",{'action':"add_statement_frm",'userid':userid},function(data){
                                jQuery("#addstaementshtml").html(data);
                                var effect = 'slide';
                                var options = {direction: 'up'};
                                var duration = 500;
                                if(jQuery("#addstaementshtml").css("display")=='none'){
                                    jQuery("#addstaementshtml").show(effect, options, duration);
                                }else{
                                    jQuery("#addstaementshtml").hide(effect, options, duration);
                                }
                            })
                        });
                    });
                    function generatestatement(){            
                        var data = jQuery("#satementfrm").serialize();
                        jQuery.post("",data,function(response){
                            jQuery("#statementtbl").html(response);
                            jQuery("#profileContent").find("form").eq(2).hide();
                            jQuery("#profileContent").find("form").eq(3).hide();
                        })
                    }
                    function sendstatements(userid){
                        var moduleurl = '$moduleURL';
                        jQuery("#messageBox").html('<p style="text-align: center;"><img style="width: 20px;height: 20px;" src="'+moduleurl+'img/loading.gif" /></p>');
                        jQuery.post("",{"action":'sendsatements',"userid":userid},
                        function(response){
                            jQuery("#messageBox").html(response);
                        });
                    }
                </script>
javascript;
        return $script;
    }
}

function account_statements_and_summary() {
    global $_LANG;
    global $CONFIG;
    global $downloads_dir;
    require_once __DIR__ . '/class/class.accountsummary.php';
    $accountsummary = new AccountSummary();
    if (isset($_POST['action']) && $_POST['action'] == "add_statement_frm") {  // Ajax request to generate filter statements form
        $script = '<script type="text/javascript">
                        jQuery(function() {
                            jQuery( "#startdate" ).datepicker({
                                showOn: "button",
                                buttonImage: "images/showcalendar.gif",
                                buttonImageOnly: true,
                                dateFormat: "yy/mm/dd"
                            });    
                            jQuery( "#enddate" ).datepicker({
                                showOn: "button",
                                buttonImage: "images/showcalendar.gif",
                                buttonImageOnly: true,
                                dateFormat: "yy/mm/dd"
                            });
                        });
                  </script>
            <h1>Account Summary & Statement</h1>
            <form id="satementfrm" action="" method="post">
                <input type="hidden" name="userid" value="' . $_POST['userid'] . '" />
                <input type="hidden" name="action" value="genrate_account_statements" />
                <table class="form" width="100%" border="0" cellspacing="2" cellpadding="3">
                    <tr>
                        <td width="15%" class="fieldlabel">Start Date</td>
                        <td class="fieldarea"><input type="text" id="startdate" name="startdate" size="15" value="" class="datepick"></td>
                        <td width="15%" class="fieldlabel">End Date</td>
                        <td class="fieldarea"><input type="text" id="enddate" name="enddate" size="15" value="" class="datepick"></td>
                    </tr>
                </table>
                <div class="btn-container">
                    <input type="button" value="Filter" class="button btn btn-default" onclick="generatestatement()" />
                </div>
            </form>';
        echo $script;
        die();
    }

    if (isset($_POST['action']) && $_POST['action'] == "genrate_account_statements") {  // Ajax request to generate account statement 
        $temp = false;
        $userid = $_POST['userid'];
        $startdate = str_replace("/", "-", $_POST['startdate']);
        $enddate = str_replace("/", "-", $_POST['enddate']);
        $dueinvoices = $accountsummary->get_due_invoces_manually($userid, $enddate, $startdate);
        if (!empty($dueinvoices)) {
            $pdf = new TCPDF();
            require_once 'accountstatementpdf.php';
            $pdffile = $accountsummary->get_attachment($userid);
            if ($temp) {
                $html = '<div id="messageBox"></div>
                <table class="datatable" width="100%" border="0" cellspacing="1" cellpadding="3">
                        <tr>
                            <th width="5%">S.No</th>
                            <th>PDF Title</th>
                            <th width="10%"></th>
                            <th width="10%"></th>
                        </tr>';
                if (!empty($pdffile)) {
                    $moduleURL = $CONFIG['SystemURL'] . '/modules/addons/accountStatement/';
                    $pdfurl = $moduleURL . 'attachments/' . $pdffile;
                    $html .= '<tr>
                            <td style="text-align:center;">1</td>
                            <td><a href=' . $pdfurl . ' target="_blank">' . $pdffile . '</td>
                            <td style="text-align:center;">
                                <img style="cursor: pointer;width: 40px;height: 40px;" onclick="window.location.href=\'addonmodules.php?module=accountStatement&id=' . $userid . '\'" src="' . $moduleURL . 'img/pdfdownload.png" />
                            </td>    
                            <td style="text-align:center;">
                                <img style="cursor: pointer;width: 32px;height: 32px;" onclick="sendstatements(' . $userid . ')" src="' . $moduleURL . 'img/mail-forward.png" />
                            </td>
                        </tr>';
                } else {
                    $html .= '<tr><td colspan="4">No Records Found</td></tr>';
                }
                $html .= '</table>';
            }
        } else {
            $html = '<div id="messageBox"></div>
                    <table class="datatable" width="100%" border="0" cellspacing="1" cellpadding="3">
                        <tr>
                            <th width="5%">S.No</th>
                            <th>PDF Title</th>
                            <th width="10%"></th>
                            <th width="10%"></th>
                        </tr>
                        <tr><td colspan="4">No outstanding invoices avaliable</td></tr>
                    </table>';
        }

        echo $html;
        die();
    }

    if (isset($_POST['action']) && $_POST['action'] == 'sendsatements') {  // Send mail ajax request
        $userid = $_POST['userid'];
        $result = account_summary_sendphpmailer($userid, "admin");
        if ($result == "success") {
            $data = '<div class="successbox"><strong><span class="title">Success!</span></strong><br>Statement sent successfully.</div>';
        } else {
            $data = '<div class="errorbox"><strong><span class="title">Error!</span></strong><br>' . $result . '</div>';
        }
        echo $data;
        die();
    }
}

// Usefull functions

function account_summary_sendphpmailer($userid, $type = NULL) {    // Send mail function with pdf attachment
    global $downloads_dir;
    require_once __DIR__ . '/class/class.accountsummary.php';
    $accountsummary = new AccountSummary();
    $checkSendMail = $accountsummary->checkSendStatement();
    if ($checkSendMail == "on") {
        $pdfname = $accountsummary->get_attachment($userid);
        $copttoDir = __DIR__ . "/attachments/" . $pdfname;
        $basepath = realpath(__DIR__ . '/../../..');
        if (isset($downloads_dir)) {
            $copyformDir = $downloads_dir . '/' . $pdfname;
        } else {
           $copyformDir = $basepath . "/downloads/" . $pdfname;
        }
        $copystatus = copy($copttoDir, $copyformDir);
        if ($copystatus) {
            $accountsummary->updateAttachments($pdfname);
            $command = "sendemail";
            $postfields["messagename"] = "Account Statement";
            $postfields["id"] = $userid;
            $results = localAPI($command, $postfields);
            if ($results['result'] == "success") {
                $accountsummary->remove_attachments_downloads($pdfname);
                $result = "success";
                if ($type == "system") {
                    $description = 'Cron Job: Account Statements ' . date("Y-m-d H:i:s") . '. Email status: Success ' . 'User ID: ' . $userid;
                } else {
                    $description = 'Account Statements ' . date("Y-m-d H:i:s") . '. Email status: Success ' . 'User ID: ' . $userid;
                }
            } else {
                $result = "Error: " . $results['message'];
                if ($type == "system") {
                    $description = "Cron Job: Message could not be sentMailer Error: " . $results['message'];
                } else {
                    $description = "Message could not be sentMailer Error: " . $results['message'];
                }
            }
        } else {
            $result = "Error: Generate in Account Statement";
            if ($type == "system") {
                $description = "Cron Job: Message could not be sentMailer " . $result;
            } else {
                $description = "Message could not be sentMailer " . $result;
            }
        }
    } else {
        $result = "Error: Please enable Enable PDF Invoices fields in Addon > Account Statement";
        if ($type == "system") {
            $description = "Cron Job: Message could not be sentMailer " . $result;
        } else {
            $description = "Message could not be sentMailer " . $result;
        }
    }
    logActivity($description);
    return $result;
}

add_hook("AfterCronJob", 1, "account_summary_sendstatements");
add_hook("AdminAreaHeadOutput", 1, "add_account_statements_invoicetab");
add_hook("AdminAreaPage", 1, "account_statements_and_summary");

add_hook('ClientAreaPrimaryNavbar', 1, function (MenuItem $primaryNavbar) {
    if (!is_null($primaryNavbar->getChild('Billing'))) {
        $primaryNavbar->getChild('Billing')->addChild('accountStatement', array(
            'label' => 'Account Statement',
            'uri' => 'index.php?m=accountStatement',
            'order' => '10',
        ));
    }
}
);